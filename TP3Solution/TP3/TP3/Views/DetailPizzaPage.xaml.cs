﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP3.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP3.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPizzaPage : ContentPage
    {
        public DetailPizzaPage(DetailPizzaViewModel vm = null)
        {
            InitializeComponent();

            if (vm != null)
            {
                this.BindingContext = vm;
            }
        }
    }
}