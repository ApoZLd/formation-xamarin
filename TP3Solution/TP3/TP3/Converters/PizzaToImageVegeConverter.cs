﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using TP3.ViewModels;
using Xamarin.Forms;

namespace TP3.Converters
{
    public class PizzaToImageVegeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            PizzaViewModel pizzaViewModel = value as PizzaViewModel;

            if (pizzaViewModel == null)
            {
                return null;
            }

            string ressourceName;

            if (pizzaViewModel.IsVegetarian)
            {
                ressourceName = "vegetarian.png";
            }
            else
            {
                ressourceName = "carnivore.png";
            }

            return ImageSource.FromResource("TP3.Resources." + ressourceName, typeof(PizzaToImageVegeConverter).GetTypeInfo().Assembly);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
