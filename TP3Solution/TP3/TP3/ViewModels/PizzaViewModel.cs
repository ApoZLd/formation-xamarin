﻿using System;
using System.Collections.Generic;
using System.Text;
using TP3.Models;

namespace TP3.ViewModels
{
    public class PizzaViewModel : BaseViewModel
    {
        public Pizza Model { get; private set; }
        public string Name { get; set; }
        public bool IsVegetarian { get; set; }

        public double Price { get; set; }
        public string Ingredients { get; set; }

        public PizzaViewModel()
        {

        }

        public PizzaViewModel(Pizza model)
        {
            this.Model = model;
            this.Name = model.Name;
            this.Ingredients = model.Ingredients;
            this.Price = model.Price;
            this.IsVegetarian = model.IsVegetarian;
        }
    }
}
