﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HelloWorld.ViewModels
{
    public class DemoMVVMViewModel : BaseViewModel
    {
        private int _Compteur;
        public int Compteur
        {
            get => this._Compteur;
            set
            {
                if(this.Set(ref this._Compteur, value))
                {
                    // Si on est là, c'est que _Compteur a changé de valeur

                    // On appelle le CanExecute de la command pour rafraichir l'interface au besoin
                    this.CmdRaz.ChangeCanExecute();
                }
            }
        }

        public Command CmdIncrement { get; set; }
        public Command CmdRaz { get; set; }

        public DemoMVVMViewModel()
        {
            this.CmdIncrement = new Command(this.Increment);
            this.CmdRaz = new Command(this.Raz, this.CanRaz);
        }

        private void Increment()
        {
            this.Compteur++;
        }

        private void Raz()
        {
            this.Compteur = 0;
        }

        private bool CanRaz()
        {
            return this.Compteur != 0;
        }
    }
}
